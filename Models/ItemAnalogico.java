package Models;

/**
 * Abstract class ItemAnalogico - Items analógicos
 * 
 * @author: Rafa León
 * Date: 2016
 */
public abstract class ItemAnalogico extends Item
{
    /**
     * Constructor
     */
    public ItemAnalogico ()
    {
        setDigital (false);
    }
    
}
