package Models;

import java.io.Serializable;


/**
 * Clase abstracta para las personas que acceden a la biblioteca.
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public abstract class Person implements IAlmacenable, Serializable
{
    /**
     * Código del usuario
     */
    Integer codigo;
    
    /**
     * Nombre de la persona
     */
    String nombre;
    
    /**
     * Clave de acceso al sistema
     */
    String password;
    
    /**
     * Obtiene el nombre de la persona
     */
    public String getNombre() { return nombre; }
    
    /**
     * Establece el nombre de la persona
     */
    public void setNombre(String n) { nombre = n; }
    
    /**
     * Obtiene la clave
     */
    public String getPassword() { return password; }
    
    /**
     * Establece la clave
     */
    public void setPassword(String p) { password = p; }
    
    /**
     * Obtiene el código del usuario
     */
    public Integer getCodigo()
    {
        return codigo;
    }
    
    /**
     * Establece el código del usuario
     */
    public void setCodigo(Integer c)
    {
        codigo = c;
    }
    
    /**
     * Establece el código del usuario automáticamente
     */
    public void setCodigo()
    {
        codigo = hashCode();
    }
    
    /**
     * Lo que se mostrará en la lista
     */
    public String toString()
    {
        return getCodigo().toString() + "-" + getNombre();
    }
    
    /**
     * Comprobar si un usuario es igual que otro
     */
    public boolean equals(Person person)
    {
        return getCodigo().equals(person.getCodigo());
    }
    
}
