package Models;


/**
 * Interface para describir cualquier item de la colección
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */

public interface IDescribible
{
    String describe ();
}
