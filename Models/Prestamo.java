package Models;

import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * Clase para implementar los préstamos de la biblioteca
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class Prestamo implements IAlmacenable, Serializable
{
    /**
     * Usuario que tiene el elemento
     */
    Person usuario;
    public Person getUsuario() { return usuario; }
    public void setUsuario(Person p) { usuario = p; }
    
    /**
     * Elemento prestado
     */
    Item elemento;
    public Item getElemento() { return elemento; }
    public void setElemento(Item i) { elemento = i; }
    
    /**
     * Fecha en que se realiza el préstamo
     */
    LocalDateTime fechaPrestamo;
    public LocalDateTime getFechaPrestamo() { return fechaPrestamo; }
    public void setFechaPrestamo(LocalDateTime d) { fechaPrestamo = d; }
    
    /**
     * Fecha prevista de devolución
     */
    LocalDateTime fechaDevolver;
    public LocalDateTime getFechaDevolver() { return fechaDevolver; }
    public void setFechaDevolver(LocalDateTime d) { fechaDevolver = d; }
    
    /**
     * Fecha de devolución real
     */
    LocalDateTime fechaDevuelto;
    public LocalDateTime getFechaDevuelto() { return fechaDevuelto; }
    public void setFechaDevuelto(LocalDateTime d) { fechaDevuelto = d; }
   
    /**
     * Lo que se mostrará en la lista de préstamos
     */
    public String toString()
    {
        String text = getElemento().getTitle() + ". Prestado a: " + getUsuario().getNombre() + " el " + getFechaPrestamo().toString().substring(0, 10) + ". ";
        
        if (getFechaDevuelto() != null) {
            text += "Devuelto el " + getFechaDevuelto().toString().substring(0, 10);
        } else {
            text += "Prevista devolución para el " + getFechaDevolver().toString().substring(0, 10);
        }
        return text;
    }

    /**
     * Comprobar si un préstamo es igual a otro
     */
    public boolean equals(Prestamo prestamo)
    {
        return getElemento().equals(prestamo.getElemento()) &&
               getUsuario().equals(prestamo.getUsuario()) &&
               getFechaPrestamo().equals(prestamo.getFechaPrestamo()) &&
               getFechaDevolver() != null && getFechaDevolver().equals(prestamo.getFechaDevolver()) &&
               (getFechaDevuelto()== null && prestamo.getFechaDevuelto() == null || getFechaDevuelto().equals(prestamo.getFechaDevuelto())); 
               
    }
}
