package Models;


/**
 * Clase audio
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class Audio extends ItemDigital
{
    /**
     * Describe el audio
     */
    public String describe ()
    {
        String result = "";         
        result = getIsbn() + "-" + "Audio. " + getTitle() + " (" + getEditorial() + "). Género: " + genero.getName() + ". Tamaño: " + getSize().toString() + " Mb. Duración: " + getDuration().toString() + " mins.";
        return result;
    }
    
}
