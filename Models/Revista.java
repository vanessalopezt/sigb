package Models;


/**
 * Clase revista
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class Revista extends ItemAnalogico implements ISuscribible
{    
    /**
     * Número de revista. Obligatorio al ser ISuscribible
     */
    Integer numero;
    public Integer getNumero() { return numero;}
    public void setNumero(Integer n) { numero = n; }

    /**
     * Lo que se mostrará en la lista de revistas
     */
    public String toString()
    {
        return getIsbn() + "-" + getTitle() + " (" + getGenero().getName() + ") nº " + getNumero().toString();
    }
    
    /**
     * Describe la revista
     */
    public String describe ()
    {
        String result = "";         
        result = "Revista. " + getTitle() + " (" + getEditorial() + ") nº " + getNumero().toString() + ". Género: " + genero.getName();
        return result;
    }
    
}
