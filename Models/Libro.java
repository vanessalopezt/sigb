package Models;


/**
 * Clase Libro
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class Libro extends ItemAnalogico 
{
    /**
     * Lo que se mostrará en la lista de libros
     */
    public String toString()
    {
        return getIsbn() + "- " + getTitle() + " - " + getAutor() + " (" + getGenero().getName() + ")";
    }
    
    /**
     * Describe el libro
     */
    public String describe ()
    {
        String result = "";         
        result = "Libro. " + getTitle() + " (" + getAutor() + "). Género: " + genero.getName() + ". Número de páginas: " + getPaginas().toString();
        return result;
    }
    
    /**
     * Autor del libro
     */
    protected String autor;
    public void setAutor(String nombre) { autor = nombre; }
    public String getAutor() {  return autor; }
   
    /**
     * Número de páginas
     */
    private Integer paginas;
    public void setPaginas(Integer p) { paginas = p; }
    public Integer getPaginas() { return paginas; }
    
    
}
