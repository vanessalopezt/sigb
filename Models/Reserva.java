package Models;

import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * Implementación de las reservas.
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class Reserva implements IAlmacenable, Serializable
{
    /**
     * Fecha en que se realiza la reserva
     */
    LocalDateTime fecha;
    public LocalDateTime getFecha() { return fecha; }
    public void setFecha(LocalDateTime d) { fecha = d; }
    
    /**
     * Usuario que realiza la reserva
     */
    Person usuario;
    public Person getUsuario() { return usuario; }
    public void setUsuario(Person p) { usuario = p; }
    
    /**
     * Elemento reservado
     */
    Item elemento;
    public Item getElemento() { return elemento; }
    public void setElemento(Item i) { elemento = i; }
    
    /**
     * Lo que se verá en las listas
     */
    public String toString()
    {
        return getFecha().toString().substring(0, 10) + " - " + getUsuario().getNombre() + " - " + getElemento().getTitle();
    }
}
