package Models;


/**
 * Interface para los elementos que son suscribibles (periodicos, revistas, ...)
 * Las clases que la implementen deberán tener un campo para indicar el número
 * de cada item (Revista X nº 1, nº2, etc.).
 * Se podría haber utilizado una clase abstracta, que heredara de ItemAnalogico y ella fuera
 * la que implementara el campo número y su getter y setter, pero si en un futuro se quisieran implementar
 * suscripciones en ItemsDigitales se podría tener un problema de diseño.
 * De esta forma creo que se prevee aunque obligue a implementar en periódicos y revistas el mismo campo ya que
 * si hay que elegir entre interfaces o clases abstractas se considera mejor práctica programar contra interfaces.
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */

public interface ISuscribible
{
    public Integer getNumero();
    public void setNumero(Integer n);
    public String getTitle();
}
