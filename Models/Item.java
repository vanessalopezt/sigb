package Models;

import java.io.Serializable;


/**
 * Abstract class Item - Clase de la que heredarán todos los elementos que se pueden incluir en la biblioteca
 * 
 * @author: Rafa León
 * Date: 2016
 */
public abstract class Item implements IAlmacenable, IDescribible, Serializable
{
    /**
     * ISBN del item
     */
    protected String isbn;
    public String getIsbn() { return isbn; }
    public void setIsbn(String code) { isbn = code; }
    
    /**
     * Título del item
     */
    protected String title;
    public String getTitle () { return title; }
    public void setTitle(String tit) { title = tit; }
    
    /**
     * Indica si el item es en soporte analógico o digital
     */
    protected boolean digital;
    public boolean getDigital () { return digital; }
    public void setDigital (boolean dig) { digital = dig; }   
    

    /**
     * Género del item
     */
    protected Gender genero;
    public void setGenero(Gender g) { genero = g; }
    public Gender getGenero() { return genero; }
    
    /**
     * Editorial o distribuidora
     */
    protected String editorial;
    public void setEditorial (String ed) { editorial = ed; }
    public String getEditorial () { return editorial; }
    
    /**
     * Disponibilidad del item
     */
    protected boolean disponible;
    public void setDisponible(boolean dis) { disponible = dis; }
    public boolean getDisponible() { return disponible; }
    
    /**
     * Comprueba si un item es igual a otro
     */
     public boolean equals(Item item)
     {
         return getIsbn().equals(item.getIsbn());
     }    
}
