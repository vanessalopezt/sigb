package Views;

import javax.swing.*;

/**
 * Nuevo item suscribible
 * 
 * @author: (Rafa León)
 * Date: (@2016)
 */
public abstract class NewItemSuscribiblePage extends NewItemPage
{
    /**
     * Contructor
     */
    NewItemSuscribiblePage(Application app)
    {
        super(app);
    }
    
    /**
     * Entry para el número del item suscribible
     */
    protected JTextField numText;
    
    /**
     * Inicializar la toma de datos
     */
    @Override
    protected void configureFieldsData()
    {
        super.configureFieldsData();
        
        JLabel label = new JLabel("Nº: ", JLabel.TRAILING);
        numText = new JTextField(10);
        numText.setText("0");
        label.setLabelFor(numText);
        panel.add(label);
        panel.add(numText);
        ++numPairs;        
    }
    
    /**
     * Controlar que se introduce un número correcto
     */
    @Override
    protected boolean validateFields()
    {
        boolean result = super.validateFields();
        if (result) {
            if (!Util.SigbUtilities.tryParseInt(numText.getText())) {
                JOptionPane.showMessageDialog(null, "Debe el número del ejemplar");
                result = false;
            }            
        }
        return result;
    }
    
    /**
     * Borrar las cajas de texto
     */
    @Override
    protected void clearEntries()
    {
        super.clearEntries();
        numText.setText("0");
    }
}
