package Views;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import ViewModels.SearchViewModel;
import Models.Item;
import Util.SigbUtilities;


/**
 * Vista de la página de búsqueda rápida
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class QuickSearchPage extends JFrame
{
    /** 
     * Indica si es la búsqueda avanzada o la rápida
     */
    boolean advancedSearch;
    
    /**
     * Viewmodel de la búsqueda
     */
    SearchViewModel viewModel;
    
    /**
     * Resultados de la búsqueda
     */
    protected JTextArea resultText;

    /**
     * Entry para la el texto de búsqueda
     */
    protected JTextField searchText;
    

    /**
     * Constructor
     */
    public QuickSearchPage (boolean adv)
    {
        super();
        
        advancedSearch = adv;
        viewModel = new SearchViewModel(advancedSearch);
        initializeComponents();
    }
    
    /**
     * Inicializar los componentes de la pantalla
     */
    void initializeComponents ()
    {
        configure();
        configureSearchPage();
    }
    
    /**
     * Configuración de la ventana
     */
    void configure()
    {
        String title;
        if (advancedSearch) {
            title = "Búsqueda flexible - Introduzca cualquier texto para realizar la búsqueda";
        } else {
            title = "Búsqueda rápida - Introduzca el título a buscar";
        }
        setTitle(title);
        setSize(750, 320);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);  
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent e) {
                dispose();        
            }
        });
    }
    
    /**
     * Configuración de la página de búsqueda
     */
    void configureSearchPage ()
    {
        searchText = new JTextField(25);

        JButton okButton = new JButton("Ok");
        okButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {     
                if (!searchText.getText().isEmpty()) {
                    showResults(viewModel.search(searchText.getText()));
                } else {
                    JOptionPane.showMessageDialog(null, "Debe introducir un texto para la búsqueda");
                }
            }
        });       
        
        resultText = new JTextArea(15, 60);
        resultText.setEditable(false);
        
        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());
        getContentPane().add(panel);

        panel.add(searchText);
        panel.add(okButton);
        panel.add(resultText);           
    }
    

    /**
     * Mostrar los resultados de la búsqueda
     */
    void showResults(ArrayList<Item> list) 
    {        
        if (list.size() == 0) {
            resultText.setText("No hay resultados");
        } else {
            String text = "";
            for (Item item : list){
                text += item.describe() + System.getProperty("line.separator");
            }
            resultText.setText(text);
        }
    }
}
