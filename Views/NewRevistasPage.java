package Views;

import javax.swing.*;
import java.awt.*;
import ViewModels.RevistaViewModel;
import Models.Revista;
import Models.Gender;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


/**
 * Nuevo periodico
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class NewRevistasPage extends NewItemSuscribiblePage
{
    /**
     * Contructor
     */
    public NewRevistasPage(Application app)
    {
        super(app);
    }
    
    /**
     * Instanciar el viewmodel
     */
    void createViewModel()
    {
        viewModel = new RevistaViewModel(App);
    }
    
    /**
     * Título de la página
     */
    String getTitlePage()
    {
        return "Nuevo revista";
    }
    
    /**
     * Tamaño de la ventana
     */
    void setSizePage()
    {
        setSize(500, 180);
    }
    
    /**
     * Preparar la toma de datos
     */
    @Override
    protected void configureFieldsData()
    {
        super.configureFieldsData();
        
        putButtons();
        
        SpringUtilities.makeCompactGrid(panel,
            numPairs, 2, //rows, cols
            6, 6,        //initX, initY
            6, 6);       //xPad, yPad        
    }

    /**
     * Preparar el item para almacenarlo
     */
    protected Revista prepareItem()
    {
        // Generar un nuevo libro y darle los valores de los Entries
        Revista item = (Revista)viewModel.getType();
        item.setIsbn(isbnText.getText());
        item.setTitle(nameText.getText());
        item.setEditorial(editorialText.getText());
        item.setGenero((Gender)genderPicker.getSelectedItem());
        item.setNumero(Integer.parseInt(numText.getText()));
        return item;
    }
    
}
