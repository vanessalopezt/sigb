package Views;

import javax.swing.*;
import java.awt.*;
import ViewModels.BaseViewModel;
import ViewModels.VideoViewModel;
import Models.Video;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


/**
 * Vista de los video
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class VideosPage extends ViewBase
{
    /**
     * Contructor
     */
    public VideosPage(Application app)
    {
        super(app);
    }
    
    /**
     * Instanciar el viewmodel
     */
    void createViewModel()
    {
        viewModel = new VideoViewModel(App);
    }
    
    /**
     * Título de la ventana
     */
    String getTitlePage()
    {
        return "Videos";
    }
    
    
    /**
     * Inicializar componentes de la pantalla
     */
    void initializeComponents()
    {
        configure();

        configureMenu();
        configureList();
    }
    
}
