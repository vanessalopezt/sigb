package Views;

import javax.swing.*;
import java.awt.*;
import ViewModels.BibliotecarioViewModel;
import Models.Bibliotecario;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import Util.SigbUtilities;


/**
 * Nuevo bibliotecario
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class NewBibliotecariosPage extends NewPersonPage
{
    /**
     * Contructor
     */
    public NewBibliotecariosPage(Application app)
    {
        super(app);
    }
    
    /**
     * Instanciar el viewmodel
     */
    void createViewModel()
    {
        viewModel = new BibliotecarioViewModel(App);
    }
    
    /**
     * Título de la página
     */
    String getTitlePage()
    {
        return "Nuevo bibliotecario";
    }    
    
    /**
     * Preparar el person para almacenarlo
     */
    protected Bibliotecario preparePerson()
    {
        // Generar un nuevo usuario y darle los valores de los Entries
        Bibliotecario bib = (Bibliotecario)viewModel.getType();
        bib.setCodigo();
        bib.setNombre(nameText.getText());
        bib.setPassword(passwordText.getText());
        return bib;
    }
    
}
