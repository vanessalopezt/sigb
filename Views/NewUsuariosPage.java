package Views;

import javax.swing.*;
import java.awt.*;
import ViewModels.UsuarioViewModel;
import Models.Usuario;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import Util.SigbUtilities;


/**
 * Nuevo usuario
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class NewUsuariosPage extends NewPersonPage
{
    /**
     * Contructor
     */
    public NewUsuariosPage(Application app)
    {
        super(app);
    }
    
    /**
     * Instanciar el viewmodel
     */
    void createViewModel()
    {
        viewModel = new UsuarioViewModel(App);
    }
    
    /**
     * Título de la página
     */
    String getTitlePage()
    {
        return "Nuevo usuario";
    }    
    
    /**
     * Preparar el person para almacenarlo
     */
    protected Usuario preparePerson()
    {
        // Generar un nuevo usuario y darle los valores de los Entries
        Usuario user = (Usuario)viewModel.getType();
        user.setCodigo();
        user.setNombre(nameText.getText());
        user.setPassword(passwordText.getText());
        return user;
    }
    
}
