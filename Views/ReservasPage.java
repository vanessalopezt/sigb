package Views;

import ViewModels.ReservaViewModel;
import Models.IAlmacenable;
import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * Vista de las reservas
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class ReservasPage extends ViewBase
{
    /**
     * Contructor
     */
    public ReservasPage(Application app)
    {
        super(app);
    }
    
    /**
     * Instanciar el viewmodel
     */
    void createViewModel()
    {
        viewModel = new ReservaViewModel(App);
    }
    
    /**
     * Título de la ventana
     */
    String getTitlePage()
    {
        return "Reservas";
    }
    
    /**
     * Inicializar componentes de la pantalla
     */
    void initializeComponents()
    {
        configure();

        configureMenu();
        configureList();
    }

    /**
     * Configurar el menú
     */
    protected void configureMenu()
    {
        JMenuBar barra = new JMenuBar();
        setJMenuBar(barra);
        JMenu menu = new JMenu("Reservas");
        barra.add(menu);
        
        JMenuItem item;
        item = new JMenuItem("Anular reserva");
        item.addActionListener(new ActionListener () {
            public void actionPerformed(ActionEvent e) {
                OnDeleteClick((IAlmacenable)list.getSelectedValue());
            }
        });
        menu.add(item);   
    }
}
