package ViewModels;

import Models.Gender;
import javax.swing.DefaultComboBoxModel;
import java.util.Iterator;
import javax.swing.DefaultListModel;
import Views.Application;

/**
 * Modelo de la vista de los géneros
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class GenderViewModel extends BaseViewModel<Gender>
{  
    /**
     * Constructor
     */
    public GenderViewModel(Application app)
    {
        super(app);
    }
    
    /**
     * Implementación del método abstracto de la superclase que devuelve una instancia IAlmacenable 
     */
    public Gender getType()
    {
        return new Gender();
    }

    /**
     * Devuelve la lista para el combo de géneros
     */
    public DefaultComboBoxModel<Gender> getGenderList()
    {
        DefaultComboBoxModel<Gender> list = new DefaultComboBoxModel<>();
        Object[] arr = getData().toArray();
        for (int i = 0; i < arr.length; i++) {
            list.addElement((Gender)arr[i]);
        }
        // Asegurar el que no haya elementos en generos
        if (list.getSize() == 0) {
            Gender g = new Gender();
            g.setName("Ninguno");
            list.addElement(g);
        }
        return list;
    }

}
