package ViewModels;

import Models.*;
import java.util.ArrayList;
import javax.swing.DefaultListModel;


/**
 * Viewmodel para las busquedas
 * 
 * @author (Rafa Leon) 
 * @version (@2016)
 */
public class SearchViewModel
{
    /**
     * Indica si es la búsqueda avanzada o la rápida
     */
    boolean advancedSearch;
    public boolean isAdvancedSearch ()
    {
        return advancedSearch;
    }
    
    /**
     * Para el acceso a los datos
     */
    protected Database<IAlmacenable> database;
    
    /**
     * Constructores
     */
    public SearchViewModel()
    {
        advancedSearch = false;
        database = new Database<IAlmacenable>();
    }
    
    public SearchViewModel(boolean adv)
    {
        this();
        advancedSearch = adv;
    }
    
    
    /**
     * Búsqueda
     */
    public ArrayList<Item> search(String param)
    {
        ArrayList<Item> result = new ArrayList<> ();
        
        for (IAlmacenable type : getTypes()) {
            DefaultListModel<IAlmacenable> data = database.loadData(type);
            
            if (data != null) {
                for (Object o : data.toArray()) {
                    Item item = (Item)o;
                    if (check(item, param)) {
                        result.add(item);
                    }
                }
            }
        }
        return result;
    }
    
    /**
     * Comprueba si Item contiene a substring en título si es búsqueda sencilla o en los campos comunes de items si es avanzada
     * Si es un libro también busca por autor
     */
    boolean check(Item item, String substring)
    {
        boolean result;
        if (!advancedSearch) {
            result = contains(item.getTitle(), substring);
        } else {
            result = contains(item.getTitle(), substring) ||
                     contains(item.getIsbn(), substring) ||
                     contains(item.getGenero().getName(), substring) ||
                     contains(item.getEditorial(), substring) ||
                     (item instanceof Libro && contains(((Libro)item).getAutor(), substring));
        }
        return result;
    }
    
    /**
     * Devuelve si input contiene a substring con sin tener en cuenta mayús. ni minús.
     */
    boolean contains(String input, String substring)
    {
        return input.toLowerCase().contains(substring.toLowerCase());
    }
    
    /**
     * Devuelve un array con una instancia de cada tipo de item para la búsqueda para luego buscar por cada tipo
     */
    IAlmacenable[] getTypes()
    {
        Item[] result = {
            new Libro(),
            new Revista(),
            new Periodico(),
            new Audio(),
            new Video()
        };
        return result;
    }    
}
