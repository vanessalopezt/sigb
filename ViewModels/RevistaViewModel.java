package ViewModels;

import Models.Revista;
import Views.Application;


/**
 * Modelo de la vista de los revistas
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class RevistaViewModel extends BaseViewModel<Revista>
{
    /**
     * Constructor
     */
    public RevistaViewModel(Application app)
    {
        super(app);
    }
    
    /**
     * Implementación del método abstracto de la superclase que devuelve una instancia IAlmacenable 
     */
    public Revista getType()
    {
        return new Revista();
    }
}
