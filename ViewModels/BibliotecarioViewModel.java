package ViewModels;

import Models.Bibliotecario;
import Views.Application;

/**
 * Modelo de la vista de los usuarios
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class BibliotecarioViewModel extends BaseViewModel<Bibliotecario>
{
    /**
     * Constructor
     */
    public BibliotecarioViewModel(Application app)
    {
        super(app);
    }
    
    /**
     * Implementación del método abstracto de la superclase que devuelve una instancia IAlmacenable 
     */
    public Bibliotecario getType()
    {
        return new Bibliotecario();
    }    
}
